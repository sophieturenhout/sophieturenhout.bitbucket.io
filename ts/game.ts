class Game {
  //attr
  private _element: HTMLElement = document.getElementById('container');
  private _car: Car;
  private _park: park; //use an array if you have multiple gameItems of the same sort
  private _scoreboard: Scoreboard;
  private _scoreboard2: Scoreboard2;
  private _player1: Player;
  private _player2: Player;
  private _turn: number;
  private _players: Player[];

  /**
   * Create the Game class
   */
  constructor() {
    //create some GameItems
    this._car = new Car('car1', 0);
    this._park = new park('park', 0);
    this._scoreboard = new Scoreboard('scoreboard');
    this._scoreboard2 = new Scoreboard2('scoreboard2');
    this._turn = 0;
    this._player1 = new Player('player 1');
    this._player2 = new Player('player 2');
    this._players = [];
    this._players.push(this._player1);
    this._players.push(this._player2);

    //add keydown handler to the window object
    window.addEventListener('keydown', this.keyDownHandler);

    //draw is initial state
    this.draw();
  }

  /**
   * Function to detect collision between two objects
   */
  public collision(): void {
    //use elem.getBoundingClientRect() for getting the wright coordinates and measurements of the element
    const parkRect = document.getElementById('park-0').getBoundingClientRect();
    const carRect = this._car.getHTMLElement().getBoundingClientRect();
    
    if (parkRect.bottom >= carRect.top) {
    
    //Switch turns
    if(this._turn ==0){
      this._scoreboard.addScore();
      this._turn =1;
    }
    else{
      this._scoreboard2.addScore();
      this._turn = 0;
      }
      //adjust scoreboard to the next player
    document.getElementById('scoreboard').innerHTML = this._players[this._turn].name;
    //TODO old car delete from the DOM
    //console.log(this._car);
    this._car.remove(this._element);
    //adjust a new car
    this._car = new Car('car'+(this._turn+1), this._turn);
    this.draw();
    //var car = document.getElementById('Car');
    //    this._element.removeChild(car);
    } 
    else {
      console.log('no collision');
    }
  }

  /**
   * Function to draw the initial state of al living objects
   */
  public draw(): void {
    this._car.draw(this._element);
    this._park.draw(this._element);
    this._scoreboard.draw(this._element);
    this._scoreboard2.draw(this._element);
  }
  
  /**
   * Function to handle the keyboard event
   * @param {KeyboardEvent} - event
   */
  public keyDownHandler = (e: KeyboardEvent): void => {
    if (e.keyCode === 32) {
      
      //move car 50px
      this._car.move(50);
      this.update();
    }
  }

 
  /**
   * Function to update the state of all living objects
   */
  public update(): void { //function formerly known as render()
    this.collision();
    this._car.update();
    this._park.update();
    this._scoreboard.update();
    this._scoreboard2.update();
  }
 
}