/// <reference path="gameItem.ts" />

class Car extends GameItem {

    private _id: number;
    /**
    * Function to create the Car
    * @param {string} - name
    * @param {number} - xPosition
    * @param {number} - yPosition
    */
    constructor(name: string, id: number, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }

    /**
    * Function to move the Car upwards
    * @param {number} - yPosition
    */
    public move(yPosition: number): void {
        this._yPos -= yPosition;
        this._element.classList.add('driving');
    }

    public getElementId(): string {
        return `${this._name}-${this._id}`;
    }

    public remove(container:HTMLElement) : void {
        console.log("removin'...");
        var id = this.getElementId();
        console.log(id);
        const elem = document.getElementById(id);
        console.log(elem);
        container.removeChild(elem);
    }
}