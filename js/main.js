class GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this.getElementId();
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.jpg `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    getHTMLElement() {
        return this._element;
    }
    getElementId() {
        return this._name;
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class Car extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    move(yPosition) {
        this._yPos -= yPosition;
        this._element.classList.add('driving');
    }
    getElementId() {
        return `${this._name}-${this._id}`;
    }
    remove(container) {
        console.log("removin'...");
        var id = this.getElementId();
        console.log(id);
        const elem = document.getElementById(id);
        console.log(elem);
        container.removeChild(elem);
    }
}
class ParkSpot extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}-${this._id}`;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    remove(container) {
        const elem = document.getElementById(`${this._name}-${this._id}`);
        container.removeChild(elem);
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.keyDownHandler = (e) => {
            if (e.keyCode === 32) {
                this._car.move(50);
                this.update();
            }
        };
        this._car = new Car('car1', 0);
        this._park = new park('park', 0);
        this._scoreboard = new Scoreboard('scoreboard');
        this._scoreboard2 = new Scoreboard2('scoreboard2');
        this._turn = 0;
        this._player1 = new Player('player 1');
        this._player2 = new Player('player 2');
        this._players = [];
        this._players.push(this._player1);
        this._players.push(this._player2);
        window.addEventListener('keydown', this.keyDownHandler);
        this.draw();
    }
    collision() {
        const parkRect = document.getElementById('park-0').getBoundingClientRect();
        const carRect = this._car.getHTMLElement().getBoundingClientRect();
        if (parkRect.bottom >= carRect.top) {
            if (this._turn == 0) {
                this._scoreboard.addScore();
                this._turn = 1;
            }
            else {
                this._scoreboard2.addScore();
                this._turn = 0;
            }
            document.getElementById('scoreboard').innerHTML = this._players[this._turn].name;
            this._car.remove(this._element);
            this._car = new Car('car' + (this._turn + 1), this._turn);
            this.draw();
        }
        else {
            console.log('no collision');
        }
    }
    draw() {
        this._car.draw(this._element);
        this._park.draw(this._element);
        this._scoreboard.draw(this._element);
        this._scoreboard2.draw(this._element);
    }
    update() {
        this.collision();
        this._car.update();
        this._park.update();
        this._scoreboard.update();
        this._scoreboard2.update();
    }
}
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class park extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}-${this._id}`;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.jpg`;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
}
class Player {
    constructor(name) {
        this._name = name;
    }
    get name() {
        return this._name;
    }
}
class Scoreboard extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    get score() {
        return this._score;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'The score is: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    addScore() {
        this._score += 1;
    }
}
class Scoreboard2 extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    get score() {
        return this._score;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'The score is: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    addScore() {
        this._score += 1;
    }
}
//# sourceMappingURL=main.js.map